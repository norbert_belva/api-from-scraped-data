# API Scrapped 

Look up users by the network name and country by specifying mcc and mnc

## Getting Started

View the project in the Master branch to preview the code.  

### Prerequisites

Git clone the master branch
```
Git clone to your local computer to run it locally.
```

### Installing

Ensure you have Node and NPM installed on your local machine.

```
Run npm install to install all the dependemcies
```

```
Use the command node index.js or nodemon to run the application.
```

```
To test the application run your localhost on postman. Use '/secure' before visiting the API endpoints.
```


## Built With

* [NodeJs](https://nodejs.org/en/) - The JS runtime used.
* [Express](https://expressjs.com/) - The web framework used
* [NPM](https://www.npmjs.com/) - Dependency Management


## Authors

* **Norbert Anjichi** - *Initial work* - [Lupamo3](https://github.com/lupamo3)


## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE.md](LICENSE.md) file for details