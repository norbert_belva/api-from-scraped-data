const express = require('express');
const router = express.Router();

const Fetch = require('../controllers/fetch');

//routes
router.get('/', Fetch.home);
router.get('/api/:mcc/:mnc', Fetch.fetch_by_mcc_mnc);
router.get('/shapi/:mcc/:mnc', Fetch.fetch_networks_by_mnc);



module.exports = router;