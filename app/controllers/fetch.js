var json_obj = require('../models/mcc.json');

// Welcome message
exports.home = async (req, res) => {
    try {
        return res.send('Hello, welcome!');
    } catch (error) {
        return res.status('404').json({ message: error.message });
    }
};

exports.fetch_by_mcc_mnc = async (req, res) => {
    try {
        const mcc = req.params.mcc;
        const mnc = req.params.mnc;

        var js = json_obj.filter(function (chunga) { return chunga.mcc == req.params.mcc  && chunga.mnc == req.params.mnc; })

        let vet;
        for (var i = 0; i < js.length; ++i) {
            vet = 'Network Name: ' + js[i].network  + '    ' + 'COUNTRY:' + js[i].country ;
            console.log(vet)
        }

        return res.status('200').json({
            success: 'true'
        });
    }
    catch (error) {
        return res.status('404').json({
            success: 'false'
        })
    }
}

exports.fetch_networks_by_mnc = async (req, res) => {
    try {
        const mcc = req.params.mcc;
        const mnc = req.params.mnc;

        var js = json_obj.filter(function (chunga) { return chunga.mcc == req.params.mcc  || chunga.mnc == req.params.mnc; })

        let vet;
        for (var i = 0; i < js.length; ++i) {
            vet = 'Network: ' + js[i].network  + '    ' + 'MCC:' + js[i].mcc  +  '   ' + 'MNC: ' + js[i].mnc ;
            console.log(vet)
        }

        return res.status('200').json({
            success: 'true'
        })
    }

    catch (error) {
        return res.status('404').json({
            success: 'false'
        })
    }
}

